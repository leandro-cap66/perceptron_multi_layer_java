
package mlp_iris_breast;

public class MLP {
    private int qtdIn,qtdOut,qtdH;
    private double [][] wh,wo;
    
    
    public MLP(int qtdIn,int qtdH,int qtdOut){
        this.qtdIn= qtdIn ;
        this.qtdH =qtdH;
        this.qtdOut = qtdOut;
        
//        wh = new double[qtdIn +1][qtdH];
//        wo = new double[qtdH +1][qtdOut];
//        wh[0][0] = 0.01;
//        wh[0][1] = -0.15;
//        wh[1][0] = -0.05;
//        wh[1][1] = 0.13;
//        wh[2][0] = 0.08;
//        wh[2][1] = 0.17;
//        
//        wo[0][0] = -0.2;
//        wo[1][0] =  0.15;
//        wo[2][0] =  -0.05;
         
        
        //---------Pesos Aleatorios-------
        wh = new double[qtdIn +1][qtdH];
        wo = new double[qtdH +1][qtdOut];
        for (int i = 0; i < qtdIn +1; i++) {
            for (int j = 0; j < qtdH; j++) {
              wh[i][j] = (Math.random()* 0.4)-0.2;
            } 
        }
        for (int i = 0; i < qtdH + 1; i++) {
            for (int j = 0; j < qtdOut; j++) {
                   wo[i][j] = (Math.random()* 0.4)-0.2;    
            }
        }
        //--------------------------------
    
    }
    
    public  double[] treinar(double xIn[],double y[], double ni) {
        double matX[] = new double[qtdIn+1];
        matX[0] = 1;
        for (int i = 1; i < matX.length ; i++) {
            matX[i] = xIn[i-1];
        }
        double matH[] = new double[qtdH +1];
        matH[0] = 1;
        for (int j = 1; j < qtdH+1; j++) {
            double u = 0;
            for (int i = 0; i < qtdIn +1; i++) {
               u = u+matX[i]*wh[i][j-1];
            }
            matH[j] = sigmoidal(u);
        }
        double[] deltaO = new double[qtdOut];
        double[] teta = new double[qtdOut];
        for (int j = 0; j < qtdOut; j++) {
            double u = 0;
            for (int i = 0; i < qtdH+1; i++) {
                u = u+matH[i] * wo[i][j];
            }
            teta[j] = sigmoidal(u);
            deltaO[j] = teta[j]*(1-teta[j])* (y[j]-teta[j]);
        }
        double deltaH[] = new double[qtdH+1];
        for (int i = 0; i < deltaH.length; i++) {
            double soma = 0;
            for (int j = 0; j < qtdOut; j++) {
                soma = soma+deltaO[j]*wo[i][j];
            }
            deltaH[i] = matH[i]*(1-matH[i])*soma;
        }
        for (int i = 0; i < qtdIn+1; i++) {
            for (int j = 0; j < qtdH; j++) {
                wh[i][j] = wh[i][j]+(ni*deltaH[j+1]*matX[i]);
            }
        }
        for (int i = 0; i < qtdH+1; i++) {
            for (int j = 0; j < qtdOut; j++) {
                wo[i][j] = wo[i][j]+ (ni*deltaO[j]*matH[i]);
            }
        }
        
        
        return teta;
       // imprimePesos();
        //return teta;
    }

     public  double[] testar(double xIn[],double y[], double ni) {
         double matX[] = new double[qtdIn+1];
        matX[0] = 1;
        for (int i = 1; i < matX.length ; i++) {
            matX[i] = xIn[i-1];
        }
        double matH[] = new double[qtdH +1];
        matH[0] = 1;
        for (int j = 1; j < qtdH+1; j++) {
            double u = 0;
            for (int i = 0; i < qtdIn +1; i++) {
               u = u+matX[i]*wh[i][j-1];
            }
            matH[j] = sigmoidal(u);
        }
        double[] deltaO = new double[qtdOut];
        double[] teta = new double[qtdOut];
        for (int j = 0; j < qtdOut; j++) {
            double u = 0;
            for (int i = 0; i < qtdH+1; i++) {
                u = u+matH[i] * wo[i][j];
            }
            teta[j] = sigmoidal(u);
            deltaO[j] = teta[j]*(1+teta[j])* (y[j]-teta[j]);
        }
        double deltaH[] = new double[qtdH+1];
        for (int i = 0; i < deltaH.length; i++) {
            double soma = 0;
            for (int j = 0; j < qtdOut; j++) {
                soma = soma+deltaO[j]*wo[i][j];
            }
            deltaH[i] = matH[i]*(1-matH[i])*soma;
        }
//        for (int i = 0; i < qtdIn+1; i++) {
//            for (int j = 0; j < qtdH; j++) {
//                wh[i][j] = wh[i][j]+(ni*deltaH[j+1]*matX[i]);
//            }
//        }
//        for (int i = 0; i < qtdH+1; i++) {
//            for (int j = 0; j < qtdOut; j++) {
//                wo[i][j] = wo[i][j]+ (ni*deltaO[j]*matH[i]);
//            }
//        }
        
        
        return teta;
     }
    
    private double sigmoidal(double u) {
        //double resp = 1/(1+Math.exp(-u));
        double resp =1/(1+Math.pow(Math.E, -u));
        return resp;
    }
    
   /* public  void imprimePesos() {
        System.out.println("Matriz WH");
        for (int i = 0; i < wh.length; i++) {
            for (int j = 0; j < wh[0].length; j++) {
                
                System.out.print(wh[i][j]+" ");
                
            }
            System.out.println();
        }
    }*/
    
}
