package mlp_iris_breast;


/**
 *
 * @author andre
 */
public class MLP_andre {

    private int qtdIn, qtdH, qtdOut;
    private double [][] wH, wO;
    
    public MLP_andre(int qtdIn, int qtdH, int qtdOut){
        //Construtor
        this.qtdIn = qtdIn;
        this.qtdH = qtdH;
        this.qtdOut = qtdOut;
        wH = new double[qtdIn+1][qtdH];
        wO = new double[qtdH+1][qtdOut];
        //Inicializar o wH
//        wH[0][0] = 0.01;
//        wH[0][1] = -0.15;
//        wH[1][0] = -0.05;
//        wH[1][1] = 0.13;
//        wH[2][0] = 0.08;
//        wH[2][1] = 0.17;
        for (int i = 0; i < qtdIn+1; i++) {
            for (int j = 0; j < qtdH; j++) {
                wH[i][j] = (Math.random()*0.4)-0.2;
            }
        }
        //Inicializar o wO
//        wO[0][0] = -0.2;
//        wO[1][0] = 0.15;
//        wO[2][0] = -0.05;
        for (int i = 0; i < qtdH+1; i++) {
            for (int j = 0; j < qtdOut; j++) {
                wO[i][j] = (Math.random()*0.4)-0.2;
            }
        }
    }
    
    public double[] treinar(double[] xIn, double[] y,double mi, boolean imprimir){
        double[] x = new double[qtdIn+1];
        x[0] = 1;//seta o bias da primeira camada
        for (int i = 1; i < xIn.length+1; i++) {
            x[i] = xIn[i-1];
        }
        
        double[] h = new double[qtdH+1];
        h[0] = 1;//setando o bias da camada oculta
        for (int j = 1; j < qtdH+1; j++) {
            double u = 0;
            for (int i = 0; i < qtdIn+1; i++) {
                u+=x[i]*wH[i][j-1];
            }
            h[j] = 1/(1+Math.pow(Math.E, -u));
        }
        double []deltaTheta = new double[qtdOut];
        double []theta = new double[qtdOut];
        for (int j = 0; j < qtdOut; j++) {
            double u = 0;
            for (int i = 0; i < qtdH+1; i++) {
                u+=h[i]*wO[i][j];
            }
            theta[j] = 1/(1+Math.pow(Math.E, -u));
            deltaTheta[j] = theta[j] * (1-theta[j]) * (y[j] - theta[j]);
        }
        double[] deltaH = new double[qtdH + 1];
        for (int i = 1; i < qtdH+1; i++) {
            double soma = 0;
            for (int j = 0; j < qtdOut; j++) {
                soma+=deltaTheta[j]*wO[i][j];
            }
            deltaH[i] = h[i] * (1-h[i]) * soma;
        }
        //Atualizar wH(pesos da camada de entrada->oculta)
        for (int i = 0; i < qtdIn+1; i++) {
            for (int j = 0; j < qtdH; j++) {
                wH[i][j] = wH[i][j] + mi*(deltaH[j+1] * x[i]);
            }
        }
        //Atualizar wO(pesos da camada oculta->saída)
        for (int i = 0; i < qtdH+1; i++) {
            for (int j = 0; j < qtdOut; j++) {
                wO[i][j] = wO[i][j] + mi*(deltaTheta[j] * h[i]);
            }
        }
        if(imprimir){
            System.out.println("\t\tMatriz wH\t\t");
            imprimePesos(wH);
            System.out.println("\t\tMatriz wO\t\t");
            imprimePesos(wO);
        }
        return theta;
    }//fim treinar
    
    public double[] testar(double[] xIn, double[] y,double mi){
        double[] x = new double[qtdIn+1];
        x[0] = 1;//seta o bias da primeira camada
        for (int i = 1; i < xIn.length+1; i++) {
            x[i] = xIn[i-1];
        }
        
        double[] h = new double[qtdH+1];
        h[0] = 1;//setando o bias da camada oculta
        for (int j = 1; j < qtdH+1; j++) {
            double u = 0;
            for (int i = 0; i < qtdIn+1; i++) {
                u+=x[i]*wH[i][j-1];
            }
            h[j] = 1/(1+Math.pow(Math.E, -u));
        }
        double []deltaTheta = new double[qtdOut];
        double []theta = new double[qtdOut];
        for (int j = 0; j < qtdOut; j++) {
            double u = 0;
            for (int i = 0; i < qtdH+1; i++) {
                u+=h[i]*wO[i][j];
            }
            theta[j] = 1/(1+Math.pow(Math.E, -u));
            deltaTheta[j] = theta[j] * (1-theta[j]) * (y[j] - theta[j]);
        }
        double[] deltaH = new double[qtdH + 1];
        for (int i = 1; i < qtdH+1; i++) {
            double soma = 0;
            for (int j = 0; j < qtdOut; j++) {
                soma+=deltaTheta[j]*wO[i][j];
            }
            deltaH[i] = h[i] * (1-h[i]) * soma;
        }
        //Atualizar wH(pesos da camada de entrada->oculta)
//        for (int i = 0; i < qtdIn+1; i++) {
//            for (int j = 0; j < qtdH; j++) {
//                wH[i][j] = wH[i][j] + mi*(deltaH[j+1] * x[i]);
//            }
//        }
        //Atualizar wO(pesos da camada oculta->saída)
//        for (int i = 0; i < qtdH+1; i++) {
//            for (int j = 0; j < qtdOut; j++) {
//                wO[i][j] = wO[i][j] + mi*(deltaTheta[j] * h[i]);
//            }
//        }
        return theta;
    }//fim treinar

    
    private void imprimePesos(double[][] matrizDePesos){
        for (int i = 0; i <= matrizDePesos[0].length; i++) {
            if(i==0) System.out.printf("\t|");
            else{
                System.out.printf("\t%d\t|",i);
            }
        }
        System.out.println("");
        for (int i = 0; i < matrizDePesos.length; i++) {
            System.out.printf("%d\t|",i);
            for (int j = 0; j < matrizDePesos[0].length; j++) {
                System.out.printf("%f\t|",matrizDePesos[i][j]);
            }
            System.out.println("");
        }        
    }

    public double[][] getwH() {
        return wH;
    }

    public double[][] getwO() {
        return wO;
    }
    
    
    
}//fim classe
