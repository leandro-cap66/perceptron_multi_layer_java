package mlp_iris_breast;


import java.awt.Color;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

 
class GerarGrafico extends ApplicationFrame {
    
    public GerarGrafico(final String title, ArrayList<Double> erro, ArrayList<Integer> classificacao, ArrayList<Double> erro_teste, ArrayList<Integer> classificacao_teste) {

        super(title);

        final XYDataset dataset = createDataset(erro, classificacao, erro_teste, classificacao_teste);
        final JFreeChart chart = createChart(dataset);
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 500));
        setContentPane(chartPanel);
    }
    
    private XYDataset createDataset(ArrayList<Double> erro, ArrayList<Integer> classificacao, ArrayList<Double> erro_teste, ArrayList<Integer> classificacao_teste) {
        
        final XYSeries series1 = new XYSeries("Erros de aproximação - Base");
        final XYSeries series2 = new XYSeries("Erro de classificação - Base");
        final XYSeries series3 = new XYSeries("Erro de aproximação - Teste");
        final XYSeries series4 = new XYSeries("Erro de classificação - Teste");
        
        for(int x = 0; x < erro.size(); x++){
            series1.add(x, erro.get(x));
            series2.add(x, classificacao.get(x));
            series3.add(x, erro_teste.get(x));
            series4.add(x, classificacao_teste.get(x));
        }

        final XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(series1);
        dataset.addSeries(series2);
        dataset.addSeries(series3);
        dataset.addSeries(series4);
                
        return dataset;
        
    }
    
    private JFreeChart createChart(final XYDataset dataset) {
        
        final JFreeChart chart = ChartFactory.createXYLineChart(
            "Gráfico",
            "Épocas",
            "Erros",
            dataset,
            PlotOrientation.VERTICAL,
            true,
            true,
            false
        );
        
        chart.setBackgroundPaint(Color.white);
        
        final XYPlot plot = chart.getXYPlot();
        plot.setBackgroundPaint(Color.lightGray);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
        
        final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesLinesVisible(1, true);
        renderer.setSeriesShapesVisible(1, true);
        plot.setRenderer(renderer);

        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
                
        return chart;
        
    }
    
    public static void abrir(String titulo, ArrayList<Double> erro, ArrayList<Integer> classificacao, ArrayList<Double> erro_teste, ArrayList<Integer> classificacao_teste) {

        final GerarGrafico demo = new GerarGrafico(titulo, erro, classificacao, erro_teste, classificacao_teste);
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);

    }
    
}
