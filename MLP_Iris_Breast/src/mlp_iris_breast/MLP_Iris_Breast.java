
package mlp_iris_breast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;


public class MLP_Iris_Breast {

    
    public static void main(String[] args) throws FileNotFoundException, IOException {
        /*MLP mlp = new MLP(2, 2, 1);
        double xIn[] = {0,1} ;
        double y[] = {1};
        
        mlp.treinar(xIn, y, 0.3);*/

        ArrayList<String[]> amostraTreino = null;
         ArrayList<String[]> amostraTeste = null;
         Random ran = new Random();
        double ni = 0.2;
        int nEntradas = 0;
        int Nh = 0;
        int nSaida = 0;
        int Namostras = 0;
        double matY[][] = null;
        double matX[][] = null;
        //int op = menu();

        File entrada = null;
       

       

        int escolha = 0;
        Scanner scan = new Scanner(System.in);
        while(escolha==0){
            System.out.println("Escolha a ação a tomar:");
            System.out.println("1-Base IRIS");
            System.out.println("2-Base BREAST CANCER WINSCONSIN");
            
            escolha = scan.nextInt();
            switch (escolha) {
                case 1:
                    entrada = new File("iris.txt");
                    break;
                case 2:
                    entrada = new File("breast.txt");
                    break;
                default:
                    escolha = 0;
                    break;  
            }
        }
        
        
        Scanner sc = new Scanner(entrada);

        String linha = sc.nextLine();
        nEntradas = Integer.parseInt(linha);
        linha = sc.nextLine();
        Nh = Integer.parseInt(linha);
        linha = sc.nextLine();
        nSaida = Integer.parseInt(linha);
        linha = sc.nextLine();
        Namostras = Integer.parseInt(linha);
        
        MLP pc = new MLP(nEntradas, Nh, nSaida);
        //MLP_andre pc = new MLP_andre(nEntradas, Nh, nSaida);
        
        
        if(escolha == 1){
            System.out.println("Iris");
            ArrayList<String[]> classe1 = new ArrayList<>();
            ArrayList<String[]> classe2 = new ArrayList<>();
            ArrayList<String[]> classe3 = new ArrayList<>();
            
            for (int i = 0; i < Namostras; i++) {
            linha = sc.nextLine();
            String itens[] = linha.split(",");
            
            String[] todasAsAmostras = new String[itens.length+nSaida-1];
            int k = 0;
            
                for (; k < itens.length-1; k++) {
                    todasAsAmostras[k] = itens[k];
                }
                
             switch(Integer.parseInt(itens[k])){
                 case 1: 
                     todasAsAmostras[k] = Integer.toString(1);
                     todasAsAmostras[k+1] = Integer.toString(0);
                     todasAsAmostras[k+2] = Integer.toString(0);
                     classe1.add(todasAsAmostras);
                    break;
                 case 2:
                     todasAsAmostras[k] = Integer.toString(0);
                     todasAsAmostras[k+1] = Integer.toString(1);
                     todasAsAmostras[k+2] = Integer.toString(0);
                     classe2.add(todasAsAmostras);
                     break;
                 case 3:
                     todasAsAmostras[k] = Integer.toString(0);
                     todasAsAmostras[k+1] = Integer.toString(0);
                     todasAsAmostras[k+2] = Integer.toString(1);
                     classe3.add(todasAsAmostras);
                     break;
             }
            
            

        }
            
            ////////////////////////////////////
            int nTreino = (int) (classe1.size() * 0.75);
            amostraTreino = new ArrayList<>();
            amostraTeste = new ArrayList<>();
            for (int i = 0; i < nTreino; i++) {
               amostraTreino.add(classe1.remove(ran.nextInt(classe1.size())));
            }
            amostraTeste.addAll(classe1);
            nTreino = (int) (classe2.size() * 0.75);
            for (int i = 0; i < nTreino; i++) {
               amostraTreino.add(classe2.remove(ran.nextInt(classe2.size())));
            }
            amostraTeste.addAll(classe2);
            nTreino = (int) (classe3.size() * 0.75);
            for (int i = 0; i < nTreino; i++) {
                amostraTreino.add(classe3.remove(ran.nextInt(classe3.size())));
            }
            amostraTeste.addAll(classe3);
            
            matX = new double[amostraTreino.size()][nEntradas];
            matY = new double[amostraTreino.size()][nSaida];
            
            for (int j = 0; j < amostraTreino.size(); j++) {
                String[] amostra = amostraTreino.get(j);
                int i = 0;
                for (; i < nEntradas; i++) {
                    matX[j][i] = Double.parseDouble(amostra[i]);
                }
                for (; i-nEntradas < nSaida; i++) {
                   matY[j][i-nEntradas] = Double.parseDouble(amostra[i]);
                    
                }
            }
        }
        
        
        if(escolha == 2){
            System.out.println("Breast");
            ArrayList<String[]> classe1 = new ArrayList<>();
            ArrayList<String[]> classe2 = new ArrayList<>();
            //ArrayList<String[]> classe3 = new ArrayList<>();
            
            for (int i = 0; i < Namostras; i++) {
            linha = sc.nextLine();
            String itens[] = linha.split(",");
            
            String[] todasAsAmostras = new String[itens.length+nSaida-1-1];
            int k = 1;
            
                for (; k < itens.length-1; k++) {
                    todasAsAmostras[k-1] = itens[k];
                }
                
             switch(Integer.parseInt(itens[k])){
                 case 1: 
                     todasAsAmostras[k-1] = Integer.toString(1);
                     todasAsAmostras[k] = Integer.toString(0);
                     //todasAsAmostras[k+2] = Integer.toString(0);
                     classe1.add(todasAsAmostras);
                    break;
                 case 2:
                     todasAsAmostras[k-1] = Integer.toString(0);
                     todasAsAmostras[k] = Integer.toString(1);
                     //todasAsAmostras[k+2] = Integer.toString(0);
                     classe2.add(todasAsAmostras);
                     break;
             }
            
            

        }
            
            ////////////////////////////////////
            int nTreino = (int) (classe1.size() * 0.75);
            amostraTreino = new ArrayList<>();
            amostraTeste = new ArrayList<>();
            for (int i = 0; i < nTreino; i++) {
               amostraTreino.add(classe1.remove(ran.nextInt(classe1.size())));
            }
            amostraTeste.addAll(classe1);
            nTreino = (int) (classe2.size() * 0.75);
            for (int i = 0; i < nTreino; i++) {
               amostraTreino.add(classe2.remove(ran.nextInt(classe2.size())));
            }
            amostraTeste.addAll(classe2);
            
            
            matX = new double[amostraTreino.size()][nEntradas];
            matY = new double[amostraTreino.size()][nSaida];
            
            for (int j = 0; j < amostraTreino.size(); j++) {
                String[] amostra = amostraTreino.get(j);
                int i = 0;
                for (; i < nEntradas; i++) {
                    matX[j][i] = Double.parseDouble(amostra[i]);
                }
                for (; i-nEntradas < nSaida; i++) {
                   matY[j][i-nEntradas] = Double.parseDouble(amostra[i]);
                    
                }
            }
        }
        
        

        System.out.println("");
        int nEpoca = 10000;
        double erroGrafClassTreino[] = new double[nEpoca];
        double erroGrafClassTeste[] = new double[nEpoca];
        double erroGrafAprTreio[] = new double[nEpoca];
        double erroGrafAprTeste[] = new double[nEpoca];
        
        double erroAprTreino[] = new double[nEpoca];
        double erroAprTeste[] = new double[nEpoca];
        
        double maiorErroEpTreino = -1;
        double maiorErroEpTeste = -1;
        
        for (int i = 0; i < nEpoca; i++) {
            double erroEpoca = 0;
            double erroClassificacao = 0;
            double teta[] = new double[nSaida];
            double ot[] = new double[nSaida];
            for (int j = 0; j < amostraTreino.size(); j++) {
             double x[] = matX[j];
             double y[] = matY[j];
                teta = pc.treinar(x, y, ni);
               // teta = pc.treinar(x, y, ni,false);
                double erro = 0;
                for (int k = 0; k < teta.length; k++) {
                    erro += Math.abs(y[k] - teta[k]);
                }
                erroEpoca+=erro;
                for (int k = 0; k < teta.length; k++) { //Formula 3
                    if (teta[k] >= 0.5) {
                        ot[k] = 1;
                    } else {
                        ot[k] = 0;
                    }
                }
                double somatorioTH = 0;

                for (int k = 0; k < y.length; k++) {
                    somatorioTH += Math.abs(y[k] - ot[k]); //Formula 4
                }
                if (somatorioTH > 0) {
                    erroClassificacao ++;
                }
            }
            if (erroEpoca > maiorErroEpTreino) {
                maiorErroEpTreino = erroEpoca;
            }
            erroGrafClassTreino[i] = erroClassificacao / amostraTreino.size(); //Formula 6
            erroAprTreino[i] = erroEpoca;
            System.out.println("Erro da época " + (i + 1) + ": " + erroEpoca + " Erro de classificação " + (i + 1) + ": " + erroClassificacao);
        
        
        ////GRAFICO TESTE///////////////////////////////
        erroEpoca = 0;
        erroClassificacao = 0;
        teta  = new double[nSaida];
        ot  = new double[nSaida];
            for (int j = 0; j < amostraTeste.size(); j++) {
             double x[] = matX[j];
             double y[] = matY[j];
                teta = pc.testar(x, y, ni);
                double erro = 0;
                for (int k = 0; k < teta.length; k++) {
                    erro += Math.abs(y[k] - teta[k]);
                }
                erroEpoca+=erro;
                for (int k = 0; k < teta.length; k++) { //Formula 3
                    if (teta[k] >= 0.5) {
                        ot[k] = 1;
                    } else {
                        ot[k] = 0;
                    }
                }
                double somatorioTH = 0;

                for (int k = 0; k < y.length; k++) {
                    somatorioTH += Math.abs(y[k] - ot[k]); //Formula 4
                }
                if (somatorioTH > 0) {
                    erroClassificacao ++;
                }
            }
            if (erroEpoca > maiorErroEpTeste) {
                maiorErroEpTeste = erroEpoca;
            }
            erroGrafClassTeste[i] = erroClassificacao / amostraTeste.size(); //Formula 6
            erroAprTeste[i] = erroEpoca;
           // System.out.println("Erro da época " + (i + 1) + ": " + erroEpoca + " Erro de classificação " + (i + 1) + ": " + erroClassificacao);
        
        //////////////////////////////////////////////
        
        }
        for (int i = 0; i < nEpoca; i++) {
            erroGrafAprTreio[i] = erroAprTreino[i] / maiorErroEpTreino;
            erroGrafAprTeste[i] = erroAprTeste[i] / maiorErroEpTeste;
        }
//        while (op==1 && op==2) {            
//            op = menu();
//        }
       if(escolha == 1){
       DefaultCategoryDataset ds = new DefaultCategoryDataset();
        for (int i = 0; i < erroAprTreino.length/100; i++) { //erroAprTreino.length
            ds.addValue(erroGrafAprTreio[i], "Erro de aproximação Treino", i + "");
            //ds.addValue(erroGrafClassTreino[i], "Erro de classificação", i + "");
            ds.addValue(erroGrafAprTeste[i], "Erro de aproximação Teste", i + "");

        }
        JFreeChart grafico = ChartFactory.createLineChart("Gráfico\nIris\n Coeficiente ni = " + ni, "Épocas", "Erro", ds, PlotOrientation.VERTICAL, true, true, false);
        try (OutputStream arquivo = new FileOutputStream("graficoAprox.png")) {
            ChartUtilities.writeChartAsPNG(arquivo, grafico, 800, 900);
        }
        
        DefaultCategoryDataset ds2 = new DefaultCategoryDataset();
        for (int i = 0; i < erroAprTreino.length/100; i++) { //erroAprTreino.length
            ds2.addValue(erroGrafClassTreino[i], "Erro de classificação Treino", i + "");
            //ds.addValue(erroGrafClassTreino[i], "Erro de classificação", i + "");
            ds2.addValue(erroGrafClassTeste[i], "Erro de classificação Teste", i + "");

        }
        JFreeChart grafico2 = ChartFactory.createLineChart("Gráfico\nIris\n Coeficiente ni = " + ni, "Épocas", "Erro", ds2, PlotOrientation.VERTICAL, true, true, false);
        try (OutputStream arquivo = new FileOutputStream("graficoClass.png")) {
            ChartUtilities.writeChartAsPNG(arquivo, grafico2, 800, 900);
        }
       
       }
       if(escolha ==2){
       DefaultCategoryDataset ds = new DefaultCategoryDataset();
        for (int i = 0; i < erroAprTreino.length/100; i++) { //erroAprTreino.length
            ds.addValue(erroGrafAprTreio[i], "Erro de aproximação Treino", i + "");
            //ds.addValue(erroGrafClassTreino[i], "Erro de classificação", i + "");
            ds.addValue(erroGrafAprTeste[i], "Erro de aproximação Teste", i + "");

        }
        JFreeChart grafico = ChartFactory.createLineChart("Gráfico\nBreast Cancer\n Coeficiente ni = " + ni, "Épocas", "Erro", ds, PlotOrientation.VERTICAL, true, true, false);
        try (OutputStream arquivo = new FileOutputStream("graficoAprox.png")) {
            ChartUtilities.writeChartAsPNG(arquivo, grafico, 800, 900);
        }
        
        DefaultCategoryDataset ds2 = new DefaultCategoryDataset();
        for (int i = 0; i < erroAprTreino.length/100; i++) { //erroAprTreino.length
            ds2.addValue(erroGrafClassTreino[i], "Erro de classificação Treino", i + "");
            //ds.addValue(erroGrafClassTreino[i], "Erro de classificação", i + "");
            ds2.addValue(erroGrafClassTeste[i], "Erro de classificação Teste", i + "");

        }
        JFreeChart grafico2 = ChartFactory.createLineChart("Gráfico\nBreast Cancer\n Coeficiente ni = " + ni, "Épocas", "Erro", ds2, PlotOrientation.VERTICAL, true, true, false);
        try (OutputStream arquivo = new FileOutputStream("graficoClass.png")) {
            ChartUtilities.writeChartAsPNG(arquivo, grafico2, 800, 900);
        }
       
       
       
       }

//GerarGrafico gerar = new GerarGrafico("Erro Classificação Iris", erroGrafAprTreio, erroGrafClassTreino, erroGrafAprTeste, );
        
    }
    
}
